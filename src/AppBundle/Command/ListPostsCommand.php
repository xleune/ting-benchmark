<?php

namespace AppBundle\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ListPostsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('list-posts')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $response = $this->getContainer()->get('post_controller')->listAction();

        $output->writeln($response->getContent());
    }
}
