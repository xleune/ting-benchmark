<?php


namespace AppBundle\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class HomeCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('home')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $response = $this->getContainer()->get('post_controller')->indexAction(1);

        $output->writeln($response->getContent());
    }
}
