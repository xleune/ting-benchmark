<?php

namespace AppBundle\Controller;

use Doctrine\ORM\EntityRepository;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;

class DefaultController
{
    private $templating;

    private $repository;

    public function __construct(EngineInterface $templating, EntityRepository $postRepository)
    {
        $this->templating = $templating;
        $this->repository = $postRepository;
    }

    public function indexAction($id)
    {
        $post = $this->repository->find($id);

        // replace this example code with whatever you need
        return $this->templating->renderResponse('default/index.html.twig', [
            'post' => $post,
        ]);
    }

    public function listAction()
    {
        $post = $this->repository->findAll();

        // replace this example code with whatever you need
        return $this->templating->renderResponse('default/list.html.twig', [
            'posts' => $post,
        ]);
    }
}
