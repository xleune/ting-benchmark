<?php
require_once 'vendor/autoload.php';
$faker = Faker\Factory::create();

$authors = fopen('authors.csv', 'w');

for($i = 0; $i < 50; $i++) {
    $state = (rand(0, 100) > 80 ? 0 : 1);
    fputcsv($authors, [$faker->name, $faker->email, $state]);
}
fclose($authors);

$categories = fopen('categories.csv', 'w');
for ($i = 0; $i < 180; $i++) {
    fputcsv($categories, [$faker->words(3, true)]);
}
fclose($categories);

$posts = fopen('posts.csv', 'w');
for ($i= 0 ; $i < 2000; $i++) {
    fputcsv($posts, [ $faker->numberBetween(1, 50), $faker->numberBetween(1, 180), $faker->words(3, true), $faker->realText(), $faker->dateTimeThisDecade()->format('Y-m-d H:i:s'), $faker->dateTimeThisDecade()->format('Y-m-d H:i:s'), ($faker->numberBetween(1, 100) > 90 ? '1' : '0')]);
}
fclose($posts);
